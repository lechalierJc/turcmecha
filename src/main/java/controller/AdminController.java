/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;

import javax.faces.bean.RequestScoped;

//@ManagedBean(name = "adminController", eager = true)
@RequestScoped
public class AdminController implements Serializable {

   private static final long serialVersionUID = 1L;

   //@ManagedProperty(value="#{admin.pageId}")
   private String pageId;

   public String processPageViewProjects(){
      return "page";
   }

   public String processPageAddProject(){
      return "page";
   }

   public String getPageId() {
      return pageId;
   }

   public void setPageId(String pageId) {
      this.pageId = pageId;
   }
}
