/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "userController", eager = true)
@RequestScoped
public class UserController implements Serializable {

   private static final long serialVersionUID = 1L;

   @ManagedProperty(value="#{user.pageId}")
   private String pageId;

   public String processPageProjectTODO(){
      return "page";
   }

   public String processPagePageDoTask(){
      return "page";
   }
   
   public String getPageId() {
      return pageId;
   }

   public void setPageId(String pageId) {
      this.pageId = pageId;
   }
}
