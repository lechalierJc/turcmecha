/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jc
 */
public class Utils {
    public static List<String> stringToArray(String tag){
        List<String> res= new ArrayList();
        String[] tab= tag.split("\"");
        for(String s : tab){
            boolean addString= true;
            if("[".equals(s)){
                addString= false;
            } 
            if("]".equals(s)){
                addString= false;
            }
            if(",".equals(s)){
                addString= false;
            }

            if(addString){
                res.add(s);
            }
        }
        return res;
    }
}
