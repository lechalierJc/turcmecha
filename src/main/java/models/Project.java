/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jc
 */
@Entity
@Table(name = "project")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
    @NamedQuery(name = "Project.findById", query = "SELECT p FROM Project p WHERE p.id = :id"),
    @NamedQuery(name = "Project.findByUserId", query = "SELECT p FROM Project p WHERE p.userId = :userId"),
    @NamedQuery(name = "Project.findByDataType", query = "SELECT p FROM Project p WHERE p.dataType = :dataType"),
    @NamedQuery(name = "Project.findByRowsCount", query = "SELECT p FROM Project p WHERE p.rowsCount = :rowsCount"),
    @NamedQuery(name = "Project.findByAnsweredRowsCount", query = "SELECT p FROM Project p WHERE p.answeredRowsCount = :answeredRowsCount"),
    @NamedQuery(name = "Project.findByAnswered", query = "SELECT p FROM Project p WHERE p.answered = :answered"),
    @NamedQuery(name = "Project.findByAnswerType", query = "SELECT p FROM Project p WHERE p.answerType = :answerType")})
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "user_id")
    private Integer userId;
    @Size(max = 255)
    @Column(name = "dataType")
    private String dataType;
    @Column(name = "rowsCount")
    private Integer rowsCount;
    @Column(name = "answeredRowsCount")
    private Integer answeredRowsCount;
    @Column(name = "answered")
    private Boolean answered;
    @Lob
    @Size(max = 65535)
    @Column(name = "questionText")
    private String questionText;
    @Size(max = 255)
    @Column(name = "answerType")
    private String answerType;
    @Lob
    @Size(max = 65535)
    @Column(name = "answerData")
    private String answerData;

    public Project() {
    }

    public Project(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public Integer getRowsCount() {
        return rowsCount;
    }

    public void setRowsCount(Integer rowsCount) {
        this.rowsCount = rowsCount;
    }

    public Integer getAnsweredRowsCount() {
        return answeredRowsCount;
    }

    public void setAnsweredRowsCount(Integer answeredRowsCount) {
        this.answeredRowsCount = answeredRowsCount;
    }

    public Boolean getAnswered() {
        return answered;
    }

    public void setAnswered(Boolean answered) {
        this.answered = answered;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getAnswerData() {
        return answerData;
    }

    public void setAnswerData(String answerData) {
        this.answerData = answerData;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Project[ id=" + id + " ]";
    }
    
}
