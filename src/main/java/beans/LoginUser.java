/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import models.User;
import models.UserFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jc
 */
@SessionScoped
@ManagedBean
@Named(value="loginUser")
public class LoginUser {
    @EJB
    private UserFacade userFacade;
    
    @PersistenceContext
    public EntityManager em;
    
    private String name;
    private String email;
    private String password;
    private boolean isAdmin;

    public LoginUser() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public String login(){
        User usr= new User();
        usr.setName(this.name);
        usr.setEmail(this.email);
        usr.setPassword(this.password);
        usr.setRole(this.isAdmin?1:0);
        User result= userFacade.login(usr);
        String role= "";
        if(result!=null){
           this.isAdmin= result.getRole()== 1;
           HttpSession session = SessionBean.getSession();
           session.setAttribute("idUser", result. getId());
            session.setAttribute("name", result.getName());
            session.setAttribute("logged", true);
            if(result.getRole()==1){
               session.setAttribute("admin", true);
               role= "admin/";
           }else {
               session.setAttribute("admin", false);
               role= "user/";
           }
       }
        
        return role+"home.xhtml?faces-redirect=true";
    }
    
    public String logout(){
        this.email= "";
        this.isAdmin= false;
        this.name= "";
        this.password= "";
        SessionBean.resetSession();
        return "index.xhtml?faces-redirect=true";
    }
    
    public String addUser(){
        User usr= new User();
        usr.setName(this.name);
        usr.setEmail(this.email);
        usr.setPassword(this.password);
        usr.setRole(this.isAdmin?1:0);
        
        userFacade.create(usr);
        return "login.xhtml?faces-redirect=true";
    }
    public void loadUser(){
        List<User> list= userFacade.findAll();
        User usr= list.get(0);
        this.email= usr.getEmail();
        this.name= usr.getName();
        this.password= usr.getPassword();
    }
    public boolean getIsAdmin(){
        return this.isAdmin;
    }
    public void setIsAdmin(boolean admin){
        this.isAdmin= admin;
    }
}
