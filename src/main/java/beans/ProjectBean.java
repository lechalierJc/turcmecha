/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.Project;
import models.Task;
import models.User;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@Named(value="project")
public class ProjectBean {
    private Project project;
    private List<Task> tasks;

    @PersistenceContext(unitName = "cyberfull_cyberfull_war_1.0PU")
    private EntityManager em;
    
    public ProjectBean() {
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public ProjectBean(User user) {
        Query q= em.createNamedQuery("Project.findByUserId");
        q.setParameter("user_id", user.getId());
        this.project= (Project)q.getSingleResult();
        
        q= em.createNamedQuery("Task.findByProjectId");
        q.setParameter("project_id", this.project.getId());
        this.tasks= (List<Task>)q.getResultList();
        
        
    }
    
    /*public Task getTaskFinished(){
        Query q= em.createNamedQuery("Task.findByProjectId&&finished");
        q.setParameter("project_id", this.project.getId());
        this.tasks= (List<Task>)q.getResultList();
        return null;
    }*/
}
