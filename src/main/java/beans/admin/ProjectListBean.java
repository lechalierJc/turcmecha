package beans.admin;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.Project;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@RequestScoped
@Named(value="projects")
public class ProjectListBean {
    private List<Project> projects= null;

    @PersistenceContext(unitName = "cyberfull_cyberfull_war_1.0PU")
    private EntityManager em;
    
    public ProjectListBean(){
        
    }
    @PostConstruct
    public void init() {
        Query q= em.createNamedQuery("Project.findAll");
        this.projects= (List<Project>)q.getResultList();
    }
    public void myProjects() {
        System.out.println("MyProject");
        int userId= (Integer)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idUser");
        System.out.println(userId);
        Query q= em.createNamedQuery("Project.findByUserId");
        q.setParameter("userId", userId);
        this.projects= (List<Project>)q.getResultList();
        System.out.println(this.projects);
    }

    public List<Project> getProject() {
        return projects;
    }

    public List<Project> getProjects() {
        return projects;
    }
    
}
