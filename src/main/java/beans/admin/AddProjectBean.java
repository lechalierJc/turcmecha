/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.admin;

import filters.Utils;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import models.Project;
import models.ProjectFacade;
import models.Task;
import models.TaskFacade;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@Named(value="addProject")
public class AddProjectBean {
    private String question;
    private String taskToDo;
    private String answerData;
    private String dataType;
    private String ressourceType;
    private int lenTaskToDo;
    private int lenAnswerType;
    
    @EJB
    private ProjectFacade projectFacade;
    @EJB
    private TaskFacade taskFacade;
    
    public void sendWork(){
        int idUser= (Integer)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idUser");
        Project prj= new Project();
        prj.setUserId(idUser);
        prj.setQuestionText(this.question);
        prj.setDataType(this.ressourceType);
        
        prj.setAnswerData(this.answerData);
        prj.setAnswerType(this.dataType);
        prj.setAnswered(Boolean.FALSE);
        
        prj.setAnsweredRowsCount(0);
        
        prj.setRowsCount(this.lenTaskToDo);
        
        int idPrj;
        idPrj = ((Project)projectFacade.create(prj)).getId();
        //TODO add a line for all data
        List<String> tab= Utils.stringToArray(this.taskToDo);
        for (Iterator i = tab.iterator(); i.hasNext();) {
            String taskText= (String)i.next();
            Task task= new Task();
            task.setData(taskText);
            task.setAnswer(null);
            task.setAnswered(Boolean.FALSE);
            task.setLocked(Boolean.FALSE);
            task.setProjectId(idPrj);
            task.setUserId(null);
            taskFacade.create(task);
        }
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getTaskToDo() {
        return taskToDo;
    }

    public void setTaskToDo(String taskToDo) {
        this.taskToDo = taskToDo;
    }

    public String getAnswerData() {
        return answerData;
    }

    public void setAnswerData(String anwserData) {
        this.answerData = anwserData;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getRessourceType() {
        return ressourceType;
    }

    public void setRessourceType(String ressourceType) {
        this.ressourceType = ressourceType;
    }

    public int getLenTaskToDo() {
        return lenTaskToDo;
    }

    public void setLenTaskToDo(int lenTaskToDo) {
        this.lenTaskToDo = lenTaskToDo;
    }

    public int getLenAnswerType() {
        return lenAnswerType;
    }

    public void setLenAnswerType(int lenAnswerType) {
        this.lenAnswerType = lenAnswerType;
    }
    
}
