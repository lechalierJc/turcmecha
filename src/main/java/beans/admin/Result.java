/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.admin;

import filters.Utils;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.Project;
import models.Task;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@Named(value="result")
public class Result {
    @PersistenceContext(unitName = "cyberfull_cyberfull_war_1.0PU")
    private EntityManager em;
    
    private Project project;
    private List<Task> tasks;
    private List<String> answerElements;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public Result(){
        
    }
    public String buildResult(int projectId){
        Query q= em.createNamedQuery("Project.findById");
        q.setParameter("id", projectId);
        this.project= (Project)q.getSingleResult();
        
        Query q2= em.createNamedQuery("Task.findByProjectId");
        q2.setParameter("projectId", projectId);
        this.tasks= (List<Task>)q2.getResultList();
        System.out.println((List<Task>)q2.getResultList());
        
        this.answerElements= Utils.stringToArray(this.project.getAnswerData());
     
        return "getResult";
    }

    public Project getProject() {
        return project;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public List<String> getAnswerElements() {
        return answerElements;
    }
    
    
}
