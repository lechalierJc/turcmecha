package beans.user;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.Project;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@RequestScoped
@Named(value="projectsTodo")
public class ProjectListToDoBean {
    private List<Project> projects= null;

    @PersistenceContext(unitName = "cyberfull_cyberfull_war_1.0PU")
    private EntityManager em;
    
    public ProjectListToDoBean(){
        
    }
    @PostConstruct
    public void init() {
        Query q= em.createNamedQuery("Project.findByAnswered");
        q.setParameter("answered", false);
        this.projects= (List<Project>)q.getResultList();
    }
    
    public String doIt(int projectId){
        System.out.println(projectId);
        //TODO charge project 
        
        
        return "doTask";
    }

    public List<Project> getProjects() {
        return projects;
    }
    
}
