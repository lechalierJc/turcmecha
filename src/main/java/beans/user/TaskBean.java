/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.user;

import filters.Utils;
import java.util.List;
import java.util.Objects;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import models.Project;
import models.ProjectFacade;
import models.Task;
import models.TaskFacade;

/**
 *
 * @author jc
 */
@Stateless
@ManagedBean
@Named(value="task")
public class TaskBean {
    private Project project;
    private List<Task> tasks;

    private Task taskDone;
    private int projectId;
    private List<String> answerElements;
    
    @PersistenceContext(unitName = "cyberfull_cyberfull_war_1.0PU")
    private EntityManager em;
    @EJB
    private ProjectFacade projectFacade;
    @EJB
    private TaskFacade taskFacade;
    
    
    public TaskBean() {
    }

    public String doIt(int projectId){
        this.projectId= projectId;
        Query q= em.createNamedQuery("Project.findById");
        q.setParameter("id", projectId);
        this.project= (Project)q.getSingleResult();
        
        Query q2= em.createNamedQuery("Task.findByProjectId&&unlocked");
        q2.setParameter("projectId", projectId);
        q2.setParameter("locked", false);
        q2.setParameter("answered", false);
        this.tasks= (List<Task>)q2.getResultList();
        System.out.println((List<Task>)q2.getResultList());
        
        if(!this.tasks.isEmpty()){
            this.taskDone= tasks.get(0);
            this.taskDone.setLocked(Boolean.TRUE);
            taskFacade.edit(this.taskDone);
            this.answerElements= Utils.stringToArray(this.project.getAnswerData());
        } else {
            this.taskDone= null;
            this.answerElements= null;
        }
        
        return "doTask";
    }
    
    public String doneTask(){
       
        //update task values
        int userId= (Integer)FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idUser");
        this.taskDone.setUserId(userId);
        this.taskDone.setAnswered(Boolean.TRUE);
        this.taskDone.setLocked(Boolean.FALSE);
        
        //update project values 
        this.project.setAnsweredRowsCount(this.project.getAnsweredRowsCount()+1);
        this.project.setAnswered(Objects.equals(this.project.getAnsweredRowsCount(), this.project.getRowsCount()));

        
        taskFacade.edit(this.taskDone);
        projectFacade.edit(this.project);
        
        Query q2= em.createNamedQuery("Task.findByProjectId&&unlocked");
        q2.setParameter("projectId", projectId);
        q2.setParameter("locked", false);
        q2.setParameter("answered", false);
        this.tasks= (List<Task>)q2.getResultList();
        System.out.println((List<Task>)q2.getResultList());
        
        if(!this.tasks.isEmpty()){
            this.taskDone= tasks.get(0);
            this.taskDone.setLocked(Boolean.TRUE);
            taskFacade.edit(this.taskDone);
            this.answerElements= Utils.stringToArray(this.project.getAnswerData());
        } else {
            this.taskDone= null;
            this.answerElements= null;
        }
        
        return "doTask";
    }

    public Task getTaskDone() {
        return taskDone;
    }

    public void setTaskDone(Task taskDone) {
        this.taskDone = taskDone;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<String> getAnswerElements() {
        return answerElements;
    }

    public void setAnswerElements(List<String> answerElements) {
        this.answerElements = answerElements;
    }
    
}
