/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jc
 */
@Stateless
public class SessionBean {

    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }
 
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }
 
    public static String getName() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if(session != null && session.getAttribute("name")!= null)
            return session.getAttribute("name").toString();
        else
            return null;
        
    }
    public static boolean getRole() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if(session != null && session.getAttribute("admin")!= null)
            return (Boolean)session.getAttribute("admin");
        else
            return false;
        
    }
    public static String getLogged() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if(session != null && session.getAttribute("logged")!= null)
            return session.getAttribute("logged").toString();
        else
            return null;
    }
    
    public static String getUserId () {
        HttpSession session = (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if(session != null && session.getAttribute("userid")!= null)
            return (String) session.getAttribute("userid");
        else
            return null;
    }
    public static void resetSession(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
    }
}

